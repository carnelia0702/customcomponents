﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomComponents
{
    public class SelectedChangedEventArgs : EventArgs
    {
        private readonly bool selected;

        /// <summary>
        /// SelectedChanged のイベントのデータを提供します。
        /// </summary>
        /// <param name="favorited"></param>
        public SelectedChangedEventArgs(bool selected)
        {
            this.selected = selected;
        }

        /// <summary>
        /// 選択されているかどうかを示す値を取得します。
        /// </summary>
        public bool Selected
        {
            get
            {
                return this.selected;
            }
        }
    }
}
