﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomComponents
{
    public class FavoritedChangedEventArgs : EventArgs
    {
        private readonly bool favorited = false;

        /// <summary>
        /// FavoritedChanged のイベントのデータを提供します。
        /// </summary>
        /// <param name="favorited"></param>
        public FavoritedChangedEventArgs(bool favorited)
        {
            this.favorited = favorited;
        }

        /// <summary>
        /// お気に入りされているかどうかを示す値を取得します。
        /// </summary>
        public bool Favorited
        {
            get
            {
                return favorited;
            }
        }
    }
}
