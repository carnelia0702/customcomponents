﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace CustomComponents
{
    public partial class Mannequin : Control
    {
        #region Field
        
        /// <summary>
        /// マネキンのイメージです。
        /// </summary>
        private readonly Image mannequin = global::CustomComponents.Properties.Resources.mannequin;

        /// <summary>
        /// マネキンの描画領域です。
        /// </summary>
        private Rectangle mannequinDst = new Rectangle();

        /// <summary>
        /// 服の描画範囲です。
        /// </summary>
        private Rectangle imageDst = new Rectangle();

        /// <summary>
        /// 服のドラッグ開始時の位置です。
        /// </summary>
        private Point imageLoc = new Point();

        /// <summary>
        /// 服の描画範囲を変更するため四方に描画する四角の描画位置です。
        /// </summary>
        private Rectangle[] sizingRect;

        /// <summary>
        /// 服の描画範囲を変更するため四方に描画する四角のサイズです。
        /// </summary>
        private readonly static int SIZING_RECTANGLE_SIZE = 7;

        /// <summary>
        /// 服を半透明に描画するための属性です。
        /// </summary>
        private readonly ImageAttributes imageAttr = new ImageAttributes();

        /// <summary>
        /// サイズ変更開始時の画像のサイズです。
        /// </summary>
        private Size imageSize;

        /// <summary>
        /// ドラッグの開始位置です。
        /// </summary>
        private Point dragStart = new Point();

        /// <summary>
        /// ドラッグ中かどうかを示します。
        /// </summary>
        private bool dragging = false;

        /// <summary>
        /// サイズ変更中かどうかを示します。
        /// </summary>
        private bool sizing = false;

        /// <summary>
        /// 服の種類を示します。
        /// </summary>
        public enum ClothCategory{Undefined, Tops, Bottoms, AllInOne, Caps, Bags};

        /// <summary>
        /// 服のイメージの縦横比を表します。
        /// </summary>
        private double ratio;

        /// <summary>
        /// 服の描画領域です。服の種類に対応しています。
        /// </summary>
        private readonly Rectangle[] drawingArea = new Rectangle[5];

        /// <summary>
        /// マネキンを描画するときの内側の余白です。
        /// </summary>
        private static readonly int PADDING = 30;

        /// <summary>
        /// サイズ変更の方向を示します。
        /// </summary>
        private int sizingType = 0;

        /// <summary>
        /// サイズ変更の開始位置です。
        /// </summary>
        private Point sizingStart;

        /// <summary>
        /// 服のイメージの初期描画時に描画領域からイメージまでのどれだけ余白をあけるかを示します。
        /// </summary>
        private static readonly int IMAGE_PADDING = 5;

        /// <summary>
        /// コントロールに入力フォーカスがあるかどうかを示す値です。
        /// </summary>
        private bool focused = false;

        // デバッグ用
        private bool debugMode = false;
        private string message = "";
        private int bx = 0, by = 0, mx = 0, my = 0;

        #endregion

        #region Field & Property

        #region Image image
        private Image image;
        /// <summary>
        /// 服のイメージを取得または設定します。
        /// </summary>
        [Category("服")]
        [Description("服のイメージです。")]
        [DefaultValue(null)]
        public Image Image
        {
            get { return this.image; }
            set
            {
                this.image = value;
                if (value != null)
                {
                    initializeImageSize();
                }
                Invalidate();
            }
        }
        #endregion

        #region String imagePath
        private String imagePath;
        /// <summary>
        /// 服のイメージのパスを取得または設定します。
        /// </summary>
        [Category("服")]
        [Description("服のイメージの読み込み先であるディスクの場所です。")]
        [DefaultValue(null)]
        public String ImagePath
        {
            get
            {
                return this.imagePath;
            }
            set
            {
                if (File.Exists(value))
                {
                    this.imagePath = value;
                    this.Image = Bitmap.FromFile(value);
                }
            }
        }
        #endregion

        #region ClothCategory category
        private ClothCategory category;
        /// <summary>
        /// 服のカテゴリーを取得または設定します。
        /// </summary>
        [Category("服")]
        [Description("服のカテゴリーです。")]
        [DefaultValue(ClothCategory.Undefined)]
        public ClothCategory Category
        {
            get
            {
                return this.category;
            }
            set
            {
                this.category = value;
                if (Image != null)
                {
                    initializeImageSize();
                }
                Invalidate();
            }
        }
        #endregion

        #endregion

        #region Constructor
        public Mannequin()
        {
            InitializeComponent();
            initialize();
        }

        public Mannequin(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            initialize();
        }

        /// <summary>
        /// 初期化します。
        /// </summary>
        private void initialize()
        {
            // Initialize Field Value
            this.image = null;
            this.imagePath = null;
            this.category = ClothCategory.Undefined;

            // Initialize Image Attribute
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.Matrix00 = 1;
            colorMatrix.Matrix11 = 1;
            colorMatrix.Matrix22 = 1;
            colorMatrix.Matrix33 = 0.7f;
            colorMatrix.Matrix44 = 1;
            this.imageAttr.SetColorMatrix(colorMatrix);

            // Initialize sizing rectangles
            sizingRect = new Rectangle[4];
            for (int i = 0; i < 4; i++)
            {
                sizingRect[i] = new Rectangle(0, 0, SIZING_RECTANGLE_SIZE, SIZING_RECTANGLE_SIZE);
            }

            initializeDrawingArea();

            // Add event handler
            this.Paint += new PaintEventHandler(paint);
            this.Resize += new EventHandler(resize);
            this.MouseMove += new MouseEventHandler(mouseMove);
            this.MouseDown += new MouseEventHandler(mouseDown);
            this.MouseUp += new MouseEventHandler(mouseUp);


            // Enable Focus to this control
            this.GotFocus += gotFocus;
            this.LostFocus += lostFocus;
            
            // Enable Double Buffering
            this.DoubleBuffered = true;
        }

        #endregion

        #region EventHandler
        private void paint(object sender, PaintEventArgs e)
        {
            // draw mannequin
            {
                e.Graphics.DrawImage(mannequin, mannequinDst);
            }


            if(this.image != null && this.category != ClothCategory.Undefined){
                // draw image
                if (dragging || sizing)
                {
                    e.Graphics.DrawImage(this.image, this.imageDst, 0, 0, this.image.Width, this.image.Height, GraphicsUnit.Pixel, imageAttr);
                }
                else
                {
                    e.Graphics.DrawImage(this.image, this.imageDst);
                }

                // draw focus Rect
                if (focused)
                {
                    ControlPaint.DrawFocusRectangle(e.Graphics, this.imageDst);

                    // draw sizing rect
                    for (int i = 0; i < 4; i++)
                    {
                        ControlPaint.DrawGrabHandle(e.Graphics, sizingRect[i], true, true);
                    }
                }
            }

            // drawing area
            if (category != ClothCategory.Undefined)
            {
                Rectangle rect = drawingArea[0];
                switch (category)
                {
                    case ClothCategory.Tops:
                        rect = drawingArea[0];
                        break;
                    case ClothCategory.Bottoms:
                        rect = drawingArea[1];
                        break;
                    case ClothCategory.AllInOne:
                        rect = drawingArea[2];
                        break;
                    case ClothCategory.Caps:
                        rect = drawingArea[3];
                        break;
                    case ClothCategory.Bags:
                        rect = drawingArea[4];
                        break;
                }
                // gen mask rect
                Rectangle[] mask = new Rectangle[4];
                mask[0] = new Rectangle(0, 0, rect.Right, rect.Top);
                mask[1] = new Rectangle(0, rect.Top, rect.Left, rect.Height);
                mask[2] = new Rectangle(rect.Right, 0, this.Width, rect.Bottom);
                mask[3] = new Rectangle(0, rect.Bottom, this.Width, this.Height);
                SolidBrush maskBrush = new SolidBrush(Color.FromArgb(128, Color.Black));
                e.Graphics.FillRectangles(maskBrush, mask);
            }

            // デバッグモードがONの時のみ描画
            if (debugMode && message != null)
            {
                // メッセージを左上に表示
                e.Graphics.DrawString(message, Font, Brushes.Black, 0, 0);

                // リサイズ時の画像の端とマウス位置を表示
                e.Graphics.DrawLine(Pens.Black, this.bx, 0, this.bx, this.Height);
                e.Graphics.DrawLine(Pens.Black, 0, this.by, this.Width, this.by);
                e.Graphics.DrawLine(Pens.Red, this.mx, 0, this.mx, this.Height);
                e.Graphics.DrawLine(Pens.Red, 0, this.my, this.Width, this.my);
            }
        }

        private void resize(object sender, EventArgs e)
        {
            Mannequin mannequin = sender as Mannequin;
            if (this.mannequin != null)
            {
                initializeMannequinSize();
            }
            if (this.image != null)
            {
                initializeImageSize();
            }
            initializeDrawingArea();
        }

        private void mouseMove(object sender, MouseEventArgs e)
        {
            // サイズ変更中
            if (sizing)
            {
                int dx = 0, dy = 0;
                switch (sizingType)
                {
                    // 左上
                    case 0:
                        // 縦横どちらが近いか
                        if (imageDst.X - e.X > imageDst.Y - e.Y)
                        {
                            dx = sizingStart.X - e.X;
                            dy = (int)(dx / ratio);
                            message = String.Format("tate{0},{1}", dx, dy);
                        }
                        else
                        {
                            dy = sizingStart.Y - e.Y;
                            dx = (int)(dy * ratio);
                            message = String.Format("yoko{0},{1}", dx, dy);
                        }
                        bx = imageDst.X;
                        by = imageDst.Y;
                        mx = e.X;
                        my = e.Y;
                        imageDst.X = imageLoc.X - dx;
                        imageDst.Y = imageLoc.Y - dy;
                        imageDst.Width = imageSize.Width + dx;
                        imageDst.Height = imageSize.Height + dy;
                        break;
                    // 右上
                    case 1:
                        // 縦横どちらが近いか
                        if (e.X-(imageDst.X + imageDst.Width) > imageDst.Y - e.Y)
                        {
                            dx = e.X - sizingStart.X;
                            dy = (int)(dx / ratio);
                            message = String.Format("tate{0},{1}", dx, dy);
                        }
                        else
                        {
                            dy = sizingStart.Y - e.Y;
                            dx = (int)(dy * ratio);
                            message = String.Format("yoko{0},{1}", dx, dy);
                        }
                        bx = (imageDst.X + imageDst.Width);
                        by = imageDst.Y;
                        mx = e.X;
                        my = e.Y;
                        imageDst.X = imageLoc.X;
                        imageDst.Y = imageLoc.Y - dy;
                        imageDst.Width = imageSize.Width + dx;
                        imageDst.Height = imageSize.Height + dy;
                        break;
                    // 左下
                    case 2:
                        // 縦横どちらが近いか
                        if (imageDst.X - e.X < e.Y - (imageDst.Y + imageDst.Height))
                        {
                            dy = sizingStart.Y - e.Y;
                            dx = (int)(dy * ratio);
                            message = String.Format("tate{0},{1}", dx, dy);
                        }
                        else
                        {
                            dx = e.X - sizingStart.X;
                            dy = (int)(dx / ratio);
                            message = String.Format("yoko{0},{1}", dx, dy);
                        }
                        bx = imageDst.X;
                        by = (imageDst.Y + imageDst.Height);
                        mx = e.X;
                        my = e.Y;
                        imageDst.X = imageLoc.X + dx;
                        imageDst.Y = imageLoc.Y;
                        imageDst.Width = imageSize.Width - dx;
                        imageDst.Height = imageSize.Height - dy;
                        break;
                    // 右下
                    case 3:
                        // 縦横どちらが近いか
                        if (e.X - (imageDst.X + imageDst.Width) < e.Y - (imageDst.Y + imageDst.Height))
                        {
                            dy = e.Y - sizingStart.Y;
                            dx = (int)(dy * ratio);
                            message = String.Format("tate{0},{1}", dx, dy);
                        }
                        else
                        {
                            dx = e.X - sizingStart.X;
                            dy = (int)(dx / ratio);
                            message = String.Format("yoko{0},{1}", dx, dy);
                        }
                        bx = (imageDst.X + imageDst.Width);
                        by = (imageDst.Y + imageDst.Height);
                        mx = e.X;
                        my = e.Y;
                        imageDst.X = imageLoc.X;
                        imageDst.Y = imageLoc.Y;
                        imageDst.Width = imageSize.Width + dx;
                        imageDst.Height = imageSize.Height + dy;
                        break;
                }
                resetSizingRect();
                Invalidate();
            }
            // ドラッグ中
            else if (dragging)
            {
                imageDst.X = imageLoc.X + (e.X - dragStart.X);
                imageDst.Y = imageLoc.Y + (e.Y - dragStart.Y);
                resetSizingRect();
                Invalidate();
                // コントロール上か判定
                if (this.DisplayRectangle.Contains(e.Location))
                {
                    Cursor = Cursors.Arrow;
                }
                else
                {
                    Cursor = Cursors.No;
                }
            }
            else
            {
                bool flag = false;
                for (int i = 0; i < 4; i++)
                {
                    if (sizingRect[i].Contains(e.Location))
                    {
                        switch (i)
                        {
                            case 0:
                            case 3:
                        Cursor = Cursors.SizeNWSE;
                                break;
                            case 1:
                            case 2:
                        Cursor = Cursors.SizeNESW;
                                break;
                        }
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    // 画像上か判定
                    if (imageDst.Contains(e.Location))
                    {
                        Cursor = Cursors.SizeAll;
                    }
                    else
                    {
                        Cursor = Cursors.Arrow;
                    }
                }
            }
        }

        private void mouseDown(object sender, MouseEventArgs e)
        {
            // サイズ変更の四角の上かどうか判定
            bool flag = false;
            for (int i = 0; i < 4; i++)
            {
                if (sizingRect[i].Contains(e.Location))
                {
                    this.sizingType = i;
                    this.sizingStart = e.Location;
                    this.imageSize = imageDst.Size;
                    this.imageLoc = imageDst.Location;
                    this.sizing = true;
                    this.ratio = (double)this.imageDst.Width / this.imageDst.Height;
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                if (imageDst.Contains(e.Location))
                {
                    this.dragStart = e.Location;
                    this.imageLoc = imageDst.Location;
                    this.dragging = true;
                    Cursor = Cursors.Arrow;
                }
            }
            
            if (Focused)
            {
                if (dragging || sizing)
                {
                    focused = true;
                }
                else
                {
                    focused = false;
                }
            }
            else
            {
                this.Focus();
            }
        }

        private void mouseUp(object sender, MouseEventArgs e)
        {
            // コントロールの範囲外でクリックしたとき
            if (!this.DisplayRectangle.Contains(e.Location))
            {
                imageDst.Location = imageLoc;
                resetSizingRect();
            }
            this.bx = this.by = this.mx = this.my = -1;
            this.message = "";
            sizing = false;
            dragging = false;
            Invalidate();
        }

        private void gotFocus(object sender, EventArgs e)
        {
            focused = true;
            Invalidate();
        }

        private void lostFocus(object sender, EventArgs e)
        {
            focused = false;
            Invalidate();
        }

        #endregion

        #region Method

        /// <summary>
        /// マネキンの表示サイズを初期化します。
        /// </summary>
        private void initializeMannequinSize()
        {
            float horizontalMagnification = (float)(this.Width - PADDING * 2) / mannequin.Width;
            float verticalMagnification = (float)(this.Height - PADDING * 2) / mannequin.Height;
            float magnification = Math.Min(horizontalMagnification, verticalMagnification);

            float imageWidth = mannequin.Width * magnification;
            float imageHeight = mannequin.Height * magnification;
            float horizontalOffset = (this.Width - imageWidth) / 2;
            float verticalOffset = (this.Height - imageHeight) / 2;

            this.mannequinDst.X = (int)horizontalOffset;
            this.mannequinDst.Y = (int)verticalOffset;
            this.mannequinDst.Width = (int)imageWidth;
            this.mannequinDst.Height = (int)imageHeight;
        }

        /// <summary>
        /// 服の表示サイズを設定します。
        /// </summary>
        private void initializeImageSize()
        {
            // 画像の描画領域を取得
            Rectangle area = drawingArea[0];
            switch (category)
            {
                case ClothCategory.Tops:
                    area = drawingArea[0];
                    break;
                case ClothCategory.Bottoms:
                    area = drawingArea[1];
                    break;
                case ClothCategory.AllInOne:
                    area = drawingArea[2];
                    break;
                case ClothCategory.Caps:
                    area = drawingArea[3];
                    break;
                case ClothCategory.Bags:
                    area = drawingArea[4];
                    break;
                default:
                    area = mannequinDst;
                    break;
            }

            float horizontalMagnification = (float)(area.Width - IMAGE_PADDING * 2) / this.image.Width;
            float verticalMagnification = (float)(area.Height - IMAGE_PADDING * 2) / this.image.Height;
            float magnification = Math.Min(horizontalMagnification, verticalMagnification);

            this.imageDst.Width = (int)(this.image.Width * magnification);
            this.imageDst.Height = (int)(this.image.Height * magnification);
            this.imageDst.X = area.X + (area.Width - this.imageDst.Width) / 2;
            this.imageDst.Y = area.Y + IMAGE_PADDING;

            resetSizingRect();
        }

        /// <summary>
        /// 服の描画領域を初期化します。
        /// </summary>
        private void initializeDrawingArea()
        {
            double ratio;
            int drawingWidth, drawingHeight;

            /* 位置は画像より算出 */
            // Tops
            ratio = 437.0 / 359.0;
            drawingHeight = (int)(this.mannequinDst.Height * 0.45);
            drawingWidth = (int)(drawingHeight * ratio);
            this.drawingArea[0] = new Rectangle((this.Width - drawingWidth) / 2, (int)(this.mannequinDst.Height * 0.12) + this.mannequinDst.Y, drawingWidth, drawingHeight);

            // Bottoms
            ratio = 330.0 / 396.0;
            drawingHeight = (int)(this.mannequinDst.Height * 0.585);
            drawingWidth = (int)(drawingHeight * ratio);
            this.drawingArea[1] = new Rectangle((this.Width - drawingWidth) / 2, (int)(this.mannequinDst.Height * 0.33) + this.mannequinDst.Y, drawingWidth, drawingHeight);

            // All In One
            ratio = 437.0 / 640.0;
            drawingHeight = (int)(this.mannequinDst.Height * 0.795);
            drawingWidth = (int)(drawingHeight * ratio);
            this.drawingArea[2] = new Rectangle((this.Width - drawingWidth) / 2, (int)(this.mannequinDst.Height * 0.12) + this.mannequinDst.Y, drawingWidth, drawingHeight);

            // Caps
            ratio = 911.0 / 590.0;
            drawingHeight = (int)(this.mannequinDst.Height * 0.13);
            drawingWidth = (int)(drawingHeight * ratio);
            this.drawingArea[3] = new Rectangle((this.Width - drawingWidth) / 2, (int)(this.mannequinDst.Height * -0.06) + this.mannequinDst.Y, drawingWidth, drawingHeight); ;

            // Bags
            int center = (int)(this.mannequinDst.Width * 0.09 + mannequinDst.X);
            ratio = 676.0 / 676.0;
            drawingHeight = (int)(this.mannequinDst.Height * 0.33);
            drawingWidth = (int)(drawingHeight * ratio);
            this.drawingArea[4] = new Rectangle(center - drawingWidth / 2, (int)(this.mannequinDst.Height * 0.46) + this.mannequinDst.Y, drawingWidth, drawingHeight);
        }

        private void resetSizingRect()
        {
            // サイズ変更用四角の位置を変更
            this.sizingRect[0].Location = new Point(imageDst.X - SIZING_RECTANGLE_SIZE / 2, imageDst.Y - SIZING_RECTANGLE_SIZE / 2);
            this.sizingRect[1].Location = new Point(imageDst.X + imageDst.Width - SIZING_RECTANGLE_SIZE / 2, imageDst.Y - SIZING_RECTANGLE_SIZE / 2);
            this.sizingRect[2].Location = new Point(imageDst.X - SIZING_RECTANGLE_SIZE / 2, imageDst.Y + imageDst.Height - SIZING_RECTANGLE_SIZE / 2);
            this.sizingRect[3].Location = new Point(imageDst.X + imageDst.Width - SIZING_RECTANGLE_SIZE / 2, imageDst.Y + imageDst.Height - SIZING_RECTANGLE_SIZE / 2);
        }

        /// <summary>
        /// 服のイメージを描画領域に合わせて保存します。
        /// </summary>
        /// <param name="path">画像の保存先です。</param>
        /// <returns>画像。</returns>
        public Image Save(String path)
        {
            // 描画領域を作成
            // drawing area
            if (image != null && category != ClothCategory.Undefined)
            {
                // 画像の描画領域を取得
                Rectangle area = drawingArea[0];
                switch (category)
                {
                    case ClothCategory.Tops:
                        area = drawingArea[0];
                        break;
                    case ClothCategory.Bottoms:
                        area = drawingArea[1];
                        break;
                    case ClothCategory.AllInOne:
                        area = drawingArea[2];
                        break;
                    case ClothCategory.Caps:
                        area = drawingArea[3];
                        break;
                    case ClothCategory.Bags:
                        area = drawingArea[4];
                        break;
                }

                // 元の画像の比率に合わせる
                double ratio = (double)image.Width / imageDst.Width;

                // 描画領域上での画像の位置を計算
                Rectangle dst = new Rectangle();
                dst.X = (int)Math.Round((imageDst.X - area.X) * ratio);
                dst.Y = (int)Math.Round((imageDst.Y - area.Y)* ratio);
                dst.Width = (int)Math.Round(imageDst.Width * ratio);
                dst.Height = (int)Math.Round(imageDst.Height * ratio);

                // 保存する画像を作成
                Bitmap bitmap = new Bitmap((int)Math.Round(area.Width * ratio), (int)Math.Round(area.Height * ratio));

                Graphics g = Graphics.FromImage(bitmap);
                //g.FillRectangle(Brushes.White, 0, 0, bitmap.Width, bitmap.Height);
                g.DrawImage(image, dst);
                g.Dispose();
                bitmap.Save(path, ImageFormat.Png);
                return bitmap;
            }
            return null;
        }

        #endregion
    }
}
