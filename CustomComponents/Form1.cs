﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomComponents
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            if (combo != null)
            {
                switch (combo.SelectedIndex)
                {
                    case 0:
                        this.mannequin1.Category = Mannequin.ClothCategory.Tops;
                        //this.mannequin1.ImagePath = @"C:\Users\弘人\Dropbox\共有フォルダ\ゆらりあ\みほん\Tops.png";
                        break;
                    case 1:
                        this.mannequin1.Category = Mannequin.ClothCategory.Bottoms;
                        //this.mannequin1.ImagePath = @"C:\Users\弘人\Dropbox\共有フォルダ\ゆらりあ\みほん\Bottoms.png";
                        break;
                    case 2:
                        this.mannequin1.Category = Mannequin.ClothCategory.AllInOne;
                        //this.mannequin1.ImagePath = @"C:\Users\弘人\Dropbox\共有フォルダ\ゆらりあ\みほん\AllInOne.png";
                        break;
                    case 3:
                        this.mannequin1.Category = Mannequin.ClothCategory.Caps;
                        //this.mannequin1.ImagePath = @"C:\Users\弘人\Dropbox\共有フォルダ\ゆらりあ\みほん\k168_01.png";
                        break;
                    case 4:
                        this.mannequin1.Category = Mannequin.ClothCategory.Bags;
                        //this.mannequin1.ImagePath = @"C:\Users\弘人\Dropbox\共有フォルダ\ゆらりあ\みほん\23-5458_01.png";
                        break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.favoritedPictureBox1.Image = this.mannequin1.Save(null);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = this.textBox1.Text;
            if (System.IO.File.Exists(path))
            {
                this.mannequin1.ImagePath = path;
            }
        }

        private void textBox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] path = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (path != null && path.Length > 0 && System.IO.File.Exists(path[0]))
            {
                this.textBox1.Text = path[0];
                this.mannequin1.ImagePath = path[0];
            }
        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void favoritedPictureBox1_Click(object sender, MouseEventArgs e)
        {
            FavoritedPictureBox pictureBox = sender as FavoritedPictureBox;
            if (pictureBox != null)
            {
                pictureBox.Selected = !pictureBox.Selected;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.mannequin1.Category = Mannequin.ClothCategory.Undefined;
            this.mannequin1.Image = null;
        }
    }
}
