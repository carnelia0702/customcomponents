﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using System.Drawing;

namespace CustomComponents
{
    public partial class FavoritedPictureBox : PictureBox
    {
        #region FavoritedChangedEvent
        public delegate void FavoritedChangedEventHandler(object sender, FavoritedChangedEventArgs e);

        [Category("FavoritedPictureBox")]
        [Description("Favorited プロパティの値がコントロールで変更されたときに発生するイベントです。")]
        public event FavoritedChangedEventHandler FavoritedChanged;

        /// <summary>
        /// FavoritedChanged イベントを発生させます。
        /// </summary>
        /// <param name="e">イベント データを格納している FavoritedChangedEventArgs。</param>
        protected virtual void OnFavoritedChanged(FavoritedChangedEventArgs e)
        {
            if (FavoritedChanged != null)
            {
                FavoritedChanged(this, e);
            }
        }

        #endregion

        #region PictureMouseClickEvent

        [Category("FavoritedPictureBox")]
        [Description("マウスでコントロールの画像をクリックしたときに発生します。")]
        public event MouseEventHandler PictureMouseClick;

        /// <summary>
        /// PictureMouseClick イベントを発生させます。
        /// </summary>
        /// <param name="e">イベント データを格納している MouseEventArgs。</param>
        protected virtual void OnPictureMouseClick(MouseEventArgs e)
        {
            if (PictureMouseClick != null)
            {
                PictureMouseClick(this, e);
            }
        }

        #endregion

        #region SelectedChanged
        public delegate void SelectedChangedEventHandler(object sender, SelectedChangedEventArgs e);

        [Category("FavoritedPictureBox")]
        [Description("Selected プロパティの値がコントロールで変更されたときに発生するイベントです。")]
        public event SelectedChangedEventHandler SelectedChanged;

        /// <summary>
        /// SelectedChanged イベントを発生させます。
        /// </summary>
        /// <param name="e">イベント データを格納している SelectedChangedEventArgs。</param>
        protected virtual void OnSelectedChanged(SelectedChangedEventArgs e)
        {
            if (SelectedChanged != null)
            {
                SelectedChanged(this, e);
            }
        }

        #endregion

        #region Field & Properties

        #region bool favorited
        private bool favorited;
        
        /// <summary>
        /// お気に入りしているかどうかを示す値を取得または設定します。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("お気に入りしているかどうかを示します。")]
        [DefaultValue(false)]
        public bool Favorited
        {
            get { return this.favorited; }
            set
            {
                bool oldValue = this.favorited;
                this.favorited = value;
                if (value)
                {
                    image = global::CustomComponents.Properties.Resources.fav;
                }
                else
                {
                    image = global::CustomComponents.Properties.Resources.nonfav;
                }

                if (oldValue != value)
                {
                    OnFavoritedChanged(new FavoritedChangedEventArgs(value));
                }

                Invalidate();
            }
        }
        #endregion

        #region bool selected
        private bool selected;

        /// <summary>
        /// 選択しているかどうかを示す値を取得または設定します。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("選択しているかどうかを示します。")]
        [DefaultValue(false)]
        public bool Selected
        {
            get { return this.selected; }
            set
            {
                bool oldValue = this.selected;
                this.selected = value;
                if (oldValue != value)
                {
                    OnSelectedChanged(new SelectedChangedEventArgs(value));
                }

                Invalidate();
            }
        }
        #endregion

        #region bool visibility
        private bool visibility;

        /// <summary>
        /// お気に入りしているかどうかを示す星を表示するかどうかを取得または設定します。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("お気に入りしているかどうかを示す星を表示するかどうかを示します。")]
        [DefaultValue(true)]
        public bool Visibility
        {
            get
            {
                return this.visibility;
            }
            set
            {
                this.visibility = value;
                Invalidate();
            }
        }


        #endregion

        #region Favorite Star

        /// <summary>
        /// 表示する画像です。
        /// </summary>
        private Bitmap image;

        /// <summary>
        /// 画像を表示する位置を示す四角形です。
        /// </summary>
        private Rectangle rect;

        private const int FAVORITE_STAR_SIZE = 32;

        #endregion

        #region Selected Rectangle

        /// <summary>
        /// コントロールの選択状態を示す四角の描画色です。
        /// </summary>
        private Color strokeColor;

        /// <summary>
        /// コントロールの選択状態を示す四角の描画色です。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("画像の選択状態を示す四角の描画色です。")]
        [DefaultValue(typeof(Color), "Black")]
        public Color StrokeColor
        {
            get
            {
                return this.strokeColor;
            }
            set
            {
                this.strokeColor = value;
                Invalidate();
            }
        }

        /// <summary>
        /// コントロールの選択状態を示す四角の太さです。
        /// </summary>
        private int strokeWeight;

        /// <summary>
        /// コントロールの選択状態を示す四角の太さです。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("画像の選択状態を示す四角の太さです。")]
        [DefaultValue(10)]
        public int StrokeWeight
        {
            get
            {
                return this.strokeWeight;
            }
            set
            {
                this.strokeWeight = value;
                Invalidate();
            }
        }

        /// <summary>
        /// コントロールの選択状態を示す四角の透過度です。
        /// </summary>
        private float strokeOpacity;

        /// <summary>
        /// コントロールの選択状態を示す四角の透過度です。
        /// </summary>
        [Category("FavoritedPictureBox")]
        [Description("コントロールの選択状態を示す四角の透過度です。")]
        [DefaultValue(1.0)]
        public float StrokeOpacity
        {
            get
            {
                return this.strokeOpacity;
            }

            set
            {
                this.strokeOpacity = value;
                Invalidate();
            }
        }

        #endregion

        #endregion

        #region Constructor
        public FavoritedPictureBox()
        {
            InitializeComponent();
            initialize();
        }

        public FavoritedPictureBox(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            initialize();
        }

        /// <summary>
        /// FavoritedPictureBox を初期化します。
        /// </summary>
        private void initialize()
        {
            // initialize field
            this.favorited = false;
            this.selected = false;
            this.strokeWeight = 10;
            this.strokeColor = Color.Black;
            this.strokeOpacity = 1.0f;
            this.image = global::CustomComponents.Properties.Resources.nonfav;
            this.visibility = true;

            calcDisplayPosition();

            // Add EventHandler
            this.Paint += new PaintEventHandler(paintFavoriteStar);
            this.MouseClick += new MouseEventHandler(mouseEventHandler);
            this.MouseMove += new MouseEventHandler(mouseMoveHandler);
            this.Resize += new EventHandler(resizeEventHandler);

            // Disable DoubleClick
            SetStyle(ControlStyles.StandardDoubleClick, false);
        }

        #endregion

        #region Method

        private void calcDisplayPosition()
        {
            this.rect = new Rectangle(this.Width - FAVORITE_STAR_SIZE, 0, FAVORITE_STAR_SIZE, FAVORITE_STAR_SIZE);
        }

        #endregion

        #region EventHandler

        /// <summary>
        /// FavoritedPictureBox の描画を行います。
        /// </summary>
        /// <param name="sender">イベントのソース。</param>
        /// <param name="e">イベント データを格納している PaintEventArgs。</param>
        private void paintFavoriteStar(object sender, PaintEventArgs e)
        {
            if (Selected)
            {
                Pen pen = new Pen(Color.FromArgb((int)(255 * strokeOpacity), strokeColor), StrokeWeight);
                e.Graphics.DrawRectangle(pen, 0, 0, this.Width, this.Height);
            }

            if (Visibility)
            {
                e.Graphics.DrawImage(image, rect);
            }
        }

        /// <summary>
        /// ResizeEvent イベントが発生したときに星の描画位置を計算し再描画します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resizeEventHandler(object sender, EventArgs e)
        {
            calcDisplayPosition();
            Invalidate();
        }

        /// <summary>
        /// MouseClick イベントが発生したときに、星がクリックされたかどうかを取得します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mouseEventHandler(object sender, MouseEventArgs e)
        {
            // 星の中心座標を求める
            double r = FAVORITE_STAR_SIZE / 2.0;
            double x = this.Width - r;
            double y = r;
            double dx = x - e.X;
            double dy = y - e.Y;

            if (Visibility && dx * dx + dy * dy <= r * r)
            {
                Favorited = !Favorited;
            }
            else
            {
                OnPictureMouseClick(e);
            }
        }

        private void mouseMoveHandler(object sender, MouseEventArgs e)
        {
            // 星の中心座標を求める
            double r = FAVORITE_STAR_SIZE / 2.0;
            double x = this.Width - r;
            double y = r;
            double dx = x - e.X;
            double dy = y - e.Y;

            if (Visibility && dx * dx + dy * dy <= r * r)
            {
                Cursor = Cursors.Hand;
            }
            else
            {
                Cursor = Cursors.Arrow;
            }
        }

        #endregion
    }
}
